LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_CFLAGS += -fPIE -fopenmp
LOCAL_LDFLAGS += -fPIE -pie -fopenmp

LOCAL_MODULE := helloneon

LOCAL_SRC_FILES := helloneon.c

LOCAL_LDLIBS := -llog

APP_PIE := true

APP_PLATFORM := android-16

include $(BUILD_EXECUTABLE)
#include $(BUILD_SHARED_LIBRARY)

$(call import-module,cpufeatures)
